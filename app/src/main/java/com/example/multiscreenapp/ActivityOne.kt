package com.example.multiscreenapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity




class ActivityOne : AppCompatActivity()  {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)
        Log.d("ActivityOne", "onCreate")

        val buttonGoForward:Button = findViewById(R.id.buttonGoForward)
        buttonGoForward.setOnClickListener {
            val intent = Intent(this, ActivityTwo::class.java)
            startActivity(intent)
        }

        val buttonGoBack: Button = findViewById(R.id.buttonGoBack)
        buttonGoBack.setOnClickListener {
            finishAffinity()
        }
    }

}